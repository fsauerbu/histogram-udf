import org.scalatest.funsuite._
import dev.sauerburger.spark.histogram.BinIndex

class BinIndexSpec extends AnyFunSuite {
  test("Underflow should go to 0") {
		val binIndex = new BinIndex()
    val binEdges = List[Double](0, 5, 10, 15)

    assert(binIndex.call(-10, binEdges) == 0)
    assert(binIndex.call(-1, binEdges) == 0)
  }

  test("Values at bin edge should go to right") {
		val binIndex = new BinIndex()
    val binEdges = List[Double](0, 5, 10, 15)

    assert(binIndex.call(0, binEdges) == 1)
    assert(binIndex.call(5, binEdges) == 2)
    assert(binIndex.call(15, binEdges) == 4)
  }

  test("Bin index should start at 1") {
		val binIndex = new BinIndex()
    val binEdges = List[Double](0, 5, 10, 15)

    assert(binIndex.call(1, binEdges) == 1)
    assert(binIndex.call(7, binEdges) == 2)
    assert(binIndex.call(13, binEdges) == 3)
  }

  test("Overflow should go to edges length") {
		val binIndex = new BinIndex()
    val binEdges = List[Double](0, 5, 10, 15)

    assert(binIndex.call(20, binEdges) == 4)
    assert(binIndex.call(100, binEdges) == 4)
  }
}
