
package dev.sauerburger.spark.histogram

import org.apache.spark.sql.api.java.UDF2

class BinIndex extends UDF2[Double, List[Double], Int] {
  override def call(value: Double, binEdges: List[Double]): Int = binEdges match {
    case Nil => -1
    case x :: Nil => if (value < x) 0 else 1
    case x :: xs  => if (value < x) 0 else call(value, xs) + 1
  }
}
