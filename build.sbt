ThisBuild / scalaVersion := "2.12.13"
ThisBuild / organization := "dev.sauerburger"
ThisBuild / version := "0.1.0"

lazy val histo = (project in file("."))
  .settings(
    name := "histogram-udf",
    libraryDependencies += "org.apache.spark" %% "spark-core" % "3.1.1",
    libraryDependencies += "org.apache.spark" %% "spark-sql" % "3.1.1",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.7" % Test,
  )
